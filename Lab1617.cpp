﻿#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<windows.h>
#include<stdlib.h>
#include<time.h>
#include <ctime>
#include <math.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	srand(time(0));

	printf("Yaroslav Polinkin\n");
	const int n = 4;

	int arr[n][n];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			arr[i][j] = rand() % 100;

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			printf("%4d ", arr[i][j]);
		printf("\n");
	}

	printf("index: ");
	int j, i, ij = 0, min = 10;
	for (j = n - 1; j >= 0; j--) {
		for (i = ij; i < n; i++) {
			printf("%d %d; ", j, i);
			if (min > arr[i][j]) min = arr[i][j];
		}
		ij++;
	}

	printf("\nmin element = %d", min);

	return 0;
}
